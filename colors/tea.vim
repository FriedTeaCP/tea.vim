highlight clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "tea"

" Color Definition
let g:black   = '#070402'
let g:red     = '#841E1E'
let g:green   = '#C6CC56'
let g:yellow  = '#D3A357'
let g:blue    = '#3D4A63'
let g:brown   = '#89644B'
let g:cyan    = '#646A78'
let g:white   = '#E8E3D8'

let g:grey1   = '#222222'
let g:grey2   = '#333333'
let g:grey3   = '#444444'
let g:grey4   = '#555555'

let g:cream   = '#D9B48F'
let g:orange  = '#D68B30'

function! s:hl(group, fg, bg, attr)
  let l:cmd = 'highlight' . ' ' . a:group

  if a:fg   != '' | let l:cmd .= ' guifg=' . a:fg | endif
  if a:bg   != '' | let l:cmd .= ' guibg=' . a:bg | endif
  if a:attr != '' | let l:cmd .= ' gui=' . a:attr | endif

  execute l:cmd
endfunction


" Vim Highlighting
call s:hl('SignColumn', '', 'NONE', '')
call s:hl('CursorLine', '', 'NONE', '')
call s:hl('CursorLineNr', g:cream, 'NONE', 'Bold')
call s:hl('LineNr', g:grey3, 'NONE', 'NONE')
highlight link EndOfBuffer LineNr

call s:hl('Visual', '#222222', g:cream, 'Bold')
call s:hl('VisualNOS', g:black, g:grey3, 'Bold')
call s:hl('Search', '#222222', g:cream, 'Bold')
call s:hl('IncSearch', g:cream, g:grey3, 'Bold')

call s:hl('Pmenu', g:white, g:grey2, '')
call s:hl('PmenuSel', g:grey2, g:white, 'Bold')
call s:hl('PmenuSbar', g:grey3, g:grey3, 'Bold')
call s:hl('PmenuThumb', g:white, g:white, 'Bold')

call s:hl('TabLine', g:grey4, g:grey2, 'Bold')
call s:hl('TabLineSel', g:cream, g:grey3, 'Bold')
call s:hl('TabLineFill', g:cream, g:grey1, 'Bold')
call s:hl('VertSplit', g:grey2, 'NONE', 'Bold')

call s:hl('StatusLine', g:white, '#BB9B7B', 'Bold')
call s:hl('StatusLineNC', '#666666', g:grey2, 'NONE')

call s:hl('MatchParen', g:cream, 'NONE', 'Bold,Italic')
call s:hl('MoreMsg', g:cream, 'NONE', '')
call s:hl('Directory', g:blue, '', '')

call s:hl('netrwTreeBar', g:brown, 'NONE', 'Bold')
call s:hl('netrwClassify', g:cyan, 'NONE', 'Bold')

call s:hl('Conceal', g:grey4, g:grey2, 'Bold')

" Syntax for text/code
" List of groups are available in syntax.txt (:h syntax.txt)
call s:hl('Comment',    g:grey3,   '',   'Italic')
call s:hl('Constant',    g:yellow,   '',   '')
call s:hl('Identifier',  g:cream, '', '')
call s:hl('Functions',  g:blue, '', 'Italic')
call s:hl('Statement', g:brown, '', 'NONE')
call s:hl('PreProc', g:brown, '', '')
call s:hl('Type', g:orange, '', 'NONE')
call s:hl('Special', g:green, '', '')
call s:hl('Underlined', g:orange, '', 'Underline')
call s:hl('Question', g:green, 'None', '')
call s:hl('Todo', g:cream, 'NONE', 'Bold,Italic')
call s:hl('SpecialKey', g:cyan, '', '')
call s:hl('Title', g:green, '', '')

" Errors/Warnings
call s:hl('Error', g:red, 'NONE', 'Bold')
call s:hl('ErrorMsg', g:red, 'NONE', 'Bold')
call s:hl('WarningMsg', g:yellow, 'NONE', 'Bold')
call s:hl('NvimInternalError', g:red, 'NONE', '')

" Diff
call s:hl('DiffAdd', g:green, 'NONE', '')
call s:hl('DiffDelete', g:red, 'NONE', '')
call s:hl('DiffChange', g:yellow, 'NONE', '')
call s:hl('diffAdded', g:green, '', '')
call s:hl('diffRemoved', g:red, '', '')

" Neovim Built-in LSP "
call s:hl('LspDiagnosticsDefaultError', g:red, '', '')
call s:hl('LspDiagnosticsDefaultWarning', g:yellow, '', '')
call s:hl('LspDiagnosticsDefaultHint', g:white, '', '')


" Terminal Colors
if has('nvim')
  let g:terminal_color_0 =  g:black   " Black
  let g:terminal_color_1 =  g:red     " Red
  let g:terminal_color_2 =  g:green   " Green
  let g:terminal_color_3 =  g:yellow  " Yellow
  let g:terminal_color_4 =  g:blue    " Blue
  let g:terminal_color_5 =  g:brown   " Magenta
  let g:terminal_color_6 =  g:cyan    " Cyan
  let g:terminal_color_7 =  g:white   " White
  let g:terminal_color_8 =  g:black   " LightBlack
  let g:terminal_color_9 =  g:red     " LightRed
  let g:terminal_color_10 = g:green   " LightGreen
  let g:terminal_color_11 = g:yellow  " LightYellow
  let g:terminal_color_12 = g:blue    " LightBlue
  let g:terminal_color_13 = g:cream   " LightMagenta
  let g:terminal_color_14 = g:cyan    " LightCyan
  let g:terminal_color_15 = g:white   " LightWhite
  let g:terminal_color_foreground = g:terminal_color_7
  " let g:terminal_color_background = g:terminal_color_0
endif
